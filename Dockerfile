FROM jfloff/alpine-python as builder

RUN apk --no-cache update && apk --no-cache add make

WORKDIR /tempDir
COPY . /tempDir

RUN pip install --upgrade pip && pip install -r requirements.txt

RUN make index.html

FROM nginx:alpine
COPY --from=builder /tempDir/index.html /tempDir/styles.css /usr/share/nginx/html/
