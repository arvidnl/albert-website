index.html: index.md template.html publish.py
	python3 publish.py $< > $@

deploy: index.html
	scp styles.css index.html arvidj.eu:/var/www/arvidj.eu/albert/

docker-push:
	docker build -t nomadiclabs/albert_website .
	docker push nomadiclabs/albert_website
