Website for the Albert language.

## Deploy the website

### Testing

    docker build -t nomadiclabs/albert_website:v1 .
    docker run -d -p 80:80 nomadiclabs/albert_website:v1

### In the Wild

    docker build -t nomadiclabs/albert_website .
    docker push nomadiclabs/blog
