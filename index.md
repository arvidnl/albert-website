# Albert 🔦
### _Intermediate smart contract programming language_

Albert is an intermediate smart contract programming language
targeting [Michelson](http://tezos.gitlab.io/whitedoc/michelson.html),
the language for the [Tezos blockchain](https://tezos.com/).  Albert
is an imperative language with variables and records, abstracting the
Michelson stack. The intent of Albert is to serve as a compilation
target for high-level smart contract programming languages. The linear
type system<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>
of Albert ensures that an Albert program, compiled to the stack-based
Michelson language, properly consumes all stack values.

The compiler, type checker and parser of Albert are written in the
proof assistant [Coq](https://coq.inria.fr/). Progression and subject reduction of the typing
rules have been proved, intuitively ensuring that *well-typed programs
do not go wrong*. The current priority for the Albert project is to
*certify* the compiler. That is, proving that the Albert compiler
preserves the semantics of the compiled program.


### Examples

The following program implements a vote.  The storage contains a
`threshold`, which is the cost of casting a vote, and the current
tally in the field `votes`.

```python
type storage_ty = { threshold : mutez; votes: map string nat }

def assert_some : { opt : option nat } -> { res : nat } =
    match opt with
    None n -> failwith "None"
    | Some n -> res = n
    end

def vote :
  { param : string ; store : storage_ty } ->
  { operations : list operation ; out_storage : storage_ty } =

    (store0, store1) = dup store;
    threshold = store1.threshold;
    (threshold, threshold_copy) = dup threshold;
    am = amount;
    ok = am >= threshold;
    match ok with
      False f ->
             failwith "you are so cheap!"
      | True t -> drop t;
             state = store0.votes ;
             (state0, state1) = dup state;
             (param0, param1) = dup param;
             prevote_option = state1[param1];
             { res = prevote } = assert_some { opt = prevote_option };
             one = 1;
             postvote = prevote + one;
             postvote = (Some : [ None : unit | Some : nat ]) postvote;
             final_state =  {| state0 with param0 |-> postvote |};
             out_storage = {threshold = threshold_copy; votes = final_state};
             operations = ([] : list operation)
    end
```

<!--
 !-- Here is the compiled program to Michelson:
 !-- 
 !-- ```michelson
 !-- TODO
 !-- ```
  -->

For comparison, consider the same program hand-written in
Michelson. The hand-written version is shorter, but arguably less
readable due to the absence of variable names.

```michelson
storage (map string int %candidates);
parameter string %chosen;
code { AMOUNT; PUSH mutez 5000000; COMPARE; GT;
       IF { FAIL } {};
       DUP; DIP { CDR; DUP }; CAR; DUP;
       DIP {
             GET; ASSERT SOME;
             PUSH int 1; ADD; SOME
           };
       UPDATE; NIL operation; PAIR
     }
```

## Features


### Functions

Albert features non-recursive, simply typed functions. They are
compiled through inlining. A function can take any number of input and
output parameters. The following example defines a function that takes
an integer, and returns its magnitude in `m` and sign in `s`.

<!--
 !-- TODO: verify typing!
  -->

```python
def magnitude_sign { i : int } -> { m : nat ; s : bool } =
    (i0, i) = dup i;
    m = abs i0 ;
    s = gt i ;
```

### Variables

Albert programs abstract the stack through variables. The following
function calculates the `n`th number in the Fibonacci sequence:

<!--
 !-- TODO: verify typing!
  -->

```python
def fib : { n : nat } -> { f1 : nat } =
   f1 = 1 ;
   f2 = 1 ;
   (n0, n) = dup n ;
   b = n0 > 0 ;
   loop b do
      (f1d, f1) = dup f1 ;
      f1 = f1d + f2 ;
      (f1d, f1) = dup f1 ;
      (f2, f2) = f1d ;
      n = n - 1 ;
      (n0, n) = dup n ;
      b = n0 > 0 ;
   end
   drop b; drop n; drop f2;
```

Note the explicit variable duplication through the `dup` keyword and
the explicit variable destructions through the `drop` instruction.
The linear typing system of Albert enforces a variable usage that
respect the stack semantics of the target language
Michelson. Consequently, all whenever a variable is used twice, it
must be duplicated explicitly through `dup`. Conversely, all produced
values must be eventually consumed (explicitly by `drop` if necessary)


### Record and variant types

Albert programs can define non-recursive, simply typed records and
variant types. Such types are compiled to nested pairs and sums in
Michelson.

The following program demonstrates how a record and variants can
represent geometrical forms. The function area uses employs *pattern
matching* on the variant type `form` to calculate the area using the
formula corresponding to the form.

<!--
 !-- TODO: verify typing!
  -->

```python
type point = { x : int ; y : int } ;

def translate { p1 : point ; p2 : point } ->  { p : point } =
    p = { x = p1.x + p2.x ; y = p1.y + p2.y }

type square = { lower_left : point ; side : int } ;
type rectangle = { lower_left : point ; upper_right : point } ;

type form = [Square : square | Rectangle : rectangle]

def area ( f : form ) -> (a : nat) =
   match f with
   Square s -> a = s.side * s.side
   | Rectangle r -> a =
      a = abs(r.upper_right.x - r.lower_left.x)  *
          abs(r.upper_right.y - r.lower_left.y)
   end;
```

### Formalized and mechanized type system and semantics in Ott

The type system and semantics of Albert are fully specified using
[Ott](https://www.cl.cam.ac.uk/~pes20/ott/). The rendered PDF version
of the static and dynamic semantics is available
[here](files/albert_proposal_final.pdf).


## How to get it

1.  First, add the required dependencies:

        opam repo add coq-released https://coq.inria.fr/opam/released
        opam repo add coq-extra-dev https://coq.inria.fr/opam/extra-dev
        opam install coq>8.8 coq-ott ott menhir "coq-menhirlib>=20190626" "ocaml>=4.07.1" coq-mi-cho-coq

2. Obtain the sources:

        git clone git@gitlab.com:rafoo_/albert_proposal.git -b compiler

3.  Make the extraction:

        make extraction

<!--
 !-- TODO: this fails for me.
  -->


<!--
 !-- ## How to use it
 !-- 
 !-- TODO: ?? Unclear, should be something with the extraction.
  -->

## Examples

The Albert repository contains a series of examples, including:

-   **[auction.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/auction.alb):** An auction contract
-   **[multisig.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/multisig.alb):** The Albert version of the [multisig](http://tezos.gitlab.io/whitedoc/michelson.html#multisig-contract) contract
-   **[vote.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/vote.alb):** The voting contract demonstrated above
-   **[simple.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/simple.alb):** A `noop`-function

And a series of test contracts demonstrating the features of Albert:

-   **[string.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/string.alb):** Tests on strings
-   **[update.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/update.alb):** Tests on updates
-   **[variant.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/variant.alb):** Tests of variant types
-   **[compare.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/compare.alb):** Tests on comparisons
-   **[fail.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/fail.alb):** Tests `failwith`
-   **[functions.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/functions.alb):** Tests on functions
-   **[list.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/list.alb):** Tests on lists
-   **[loop.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/loop.alb):** Tests on loops
-   **[map.alb](https://gitlab.com/rafoo_/albert_proposal/blob/compiler/examples/map.alb):** Tests on maps


## Roadmap

 - Package for opam
 - Certify the compiler


# Further reading

 - [Basile Pesin's internship report on the design and compilation Albert](files/basile_pesin_report.pdf) and [associated slides](files/2019_M1_Basile_Pesin_soutenance.pdf)
 - [The formal specification of Albert](files/albert_proposal_final.pdf)
 - [Albert Gitlab Repo](https://gitlab.com/rafoo_/albert_proposal/commits/compiler)


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Wadler, Philip. "Linear types can change the world!." Programming concepts and methods. Vol. 3. No. 4. 1990.
